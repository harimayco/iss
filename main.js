const puppeteer = require('puppeteer-extra');
const StealthPlugin = require('puppeteer-extra-plugin-stealth')


//const AdblockerPlugin = require('puppeteer-extra-plugin-adblocker')

const csvUtil = require('csv-append-write-read');

const fs = require('fs')
const currentFolder = __dirname;
const pdfFolder = currentFolder + '/pdf';
const logsPath = currentFolder + '/logs.csv';

puppeteer.use(StealthPlugin());

run();

async function run() {

  try {
    await checkFile(logsPath);
  } catch (err) {
    await require('fs/promises').writeFile(logsPath, 'result\n', 'utf-8');
  }

  let akun = await csvUtil.read(currentFolder + '/akun.csv');
  let data = await csvUtil.read(currentFolder + '/data.csv');

  console.log(currentFolder);
  console.log('Total Akun: ' + akun.length);
  console.log('Total Data: ' + data.length);
  if (data.length < akun.length) {
    console.log("Kekurangan Data. Data Harus sama atau lebih banyak dari total akun");
    return false;
  }
  for (let i = 0; i < akun.length; i++) {
    let email = akun[i].email;
    let password = akun[i].password;
    let postdata = data[i];

    let bw = await openBrowser(i);
    login(bw.page, email, password).then(async function (page) {
      await doTask(page, postdata);
      bw.browser.close();
    })

  }
}
async function openBrowser(i) {
  const browser = await puppeteer.launch({
    executablePath: "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe",
    headless: false,
    excludeSwitches: 'enable-automation',
    args: [
      '--disable-gpu',
      `--window-position=${i * 10},0`
    ]
  });
  var [page] = await browser.pages();
  page.setDefaultNavigationTimeout(0);
  page.setDefaultTimeout(0);
  await page.setUserAgent('Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36')
  return Promise.resolve({ browser: browser, page: page });
}

async function login(page, email, password) {
  await page.goto('https://accounts.google.com/signin/v2/identifier', { waitUntil: 'networkidle2' });
  await page.waitForSelector(`#identifierId`, { visible: true });
  await page.type(`#identifierId`, email, { delay: 0 });
  await page.keyboard.press('Enter');
  //await page.waitForTimeout(1000);

  await page.waitForSelector(`#password input[type="password"]`, { visible: true });
  await page.type(`#password input[type="password"]`, password, { delay: 0 });
  // await newPage.click('#passwordNext');
  await page.keyboard.press('Enter');
  await page.waitForTimeout(1000);
  await page.waitForNavigation();

  await page.waitForRequest(request => {
    return request.url().includes('myaccount.google.com')
  });

  return Promise.resolve(page);

}

async function doTask(page, data) {

  let pdf = pdfFolder + '/' + data.pdf;
  let title = data.judul;
  let desc = data.deskripsi;

  await page.goto('https://issuu.com/signin', { waitUntil: 'networkidle0' });
  await page.click('#CybotCookiebotDialogBodyLevelButtonLevelOptinAllowAll');
  await Promise.all([
    page.$eval(`button[aria-label="Sign in with Google"]`, element =>
      element.click()
    ),
  ]);
  //console.log(page.url());
  await page.waitForRequest(request => {
    return request.url().includes('issuu.com/home/publisher')
  });

  await page.waitForTimeout(2000);

  await page.waitForSelector("input[name=file]",);
  const input = await page.$("input[name=file]");
  await input.uploadFile(pdf);

  // ngisi judul
  await page.waitForSelector('textarea[data-testid="publication-title"]');
  await page.focus('textarea[data-testid="publication-title"]');
  await page.keyboard.type(title, { delay: 0 });


  //ngisi deskripsi
  await page.waitForSelector('#description-input');
  console.log(page.url());
  await page.focus('#description-input')
  await page.keyboard.type(desc, { delay: 0 })

  // tunggu sampai file ready
  await page.waitForSelector('iframe[data-testid="preview-embed"]');
  await page.waitForTimeout(2000);
  //klik publish
  await page.waitForSelector('button[data-testid="publish-button"]');
  await page.click('button[data-testid="publish-button"]');
  await page.waitForTimeout(2000);
  await page.click('button[data-testid="publish-button"]');
  await page.waitForTimeout(3000);

  const [element] = await page.$x('//span[contains(text(), "Confirm &")]');
  //console.log(element);

  const element_parent = (await element.$x('..'))[0];
  element_parent.click();


  await page.waitForTimeout(2000);
  await page.click('button[data-testid="publish-button"]');


  await page.waitForSelector('.ixu-textfield', { visible: true });
  let e = await page.$('.ixu-textfield');
  let value = await page.evaluate(el => el.value, e)
  console.log(value);

  // write log
  await csvUtil.append({
    result: value
  }, currentFolder + '/logs.csv');

  return Promise.resolve(1);
}

async function checkFile(fileName) {
  try {
    if (fs.existsSync(fileName)) {
      //console.log(`${dataCsv} ditemukan`);
      return fileName;
    }
  } catch (err) {
    //console.log(`File ${fileName} tidak ditemukan`);
    throw Error(`File ${fileName} tidak ditemukan`);
  }
}